### STAGE 1: Build ###
FROM node:14-alpine3.15 AS build
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
#RUN npm run build --prod

### STAGE 2: Run ###
FROM nginx:1.17.1-alpine
#COPY csr.crt /etc/ssl/certs/csr.crt
#COPY key.key /etc/ssl/private/key.key
#COPY --from=build /usr/src/app/dist /usr/share/nginx/html
COPY ./dist /usr/share/nginx/html
COPY /nginx.conf  /etc/nginx/conf.d/default.conf

