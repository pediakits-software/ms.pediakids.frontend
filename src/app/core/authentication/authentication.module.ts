import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginPageComponent } from './login-page/login-page.component';
import { GenericInputModule } from 'app/shared/components/generic-input/generic-input.module';
import { GenericLoadingModule } from 'app/shared/components/generic-loading/generic-loading.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxMaskModule, IConfig } from 'ngx-mask'
const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    LoginPageComponent,

  ],
  imports: [
    CommonModule,
    AuthenticationRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
    GenericInputModule,
    GenericLoadingModule,
    NgbModule,
    NgxMaskModule.forRoot(maskConfig)
  ]
})
export class AuthenticationModule { }
