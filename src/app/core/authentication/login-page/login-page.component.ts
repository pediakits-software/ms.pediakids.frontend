import { AfterViewInit, Component, HostListener, Inject, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";
import { NgForm, FormGroup, FormControl, Validators, FormBuilder } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { NgbCarouselConfig } from "@ng-bootstrap/ng-bootstrap";
import { SecurityService } from "app/shared/auth/security.service";
import { InputType } from "app/shared/components/generic-input/generic-input.component";
import { GenericLoadingService } from "app/shared/components/generic-loading/generic-loading.service";
import { SweetAlertService } from "app/shared/services/sweet-alert.service";
import { WINDOW } from "app/shared/services/window.service";
import { Utils } from "app/shared/utils/utils.resource";

@Component({
  selector: "app-login-page",
  templateUrl: "./login-page.component.html",
  styleUrls: ["./login-page.component.scss"],
  encapsulation: ViewEncapsulation.None,
})
export class LoginPageComponent implements OnInit, AfterViewInit{
  isLoginFailed = false;
  inputType= InputType

  formGroupLogin:FormGroup;

  // CARRUSEL
  showNavigationArrows = true;
  showNavigationIndicators = false;
  images = [1, 2, 3].map((n) => `../../../../assets/img/slider/s${n}.jpg`);

  constructor(
    @Inject(WINDOW) private window: Window,

    private genericLoadingService:GenericLoadingService,
    private router: Router,
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private _sweetAlertService: SweetAlertService,
    config: NgbCarouselConfig,
    private _securityService: SecurityService
  ) {
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }
  ngAfterViewInit(): void {
    this.changeheight();
  }
  ngOnInit(): void {
   this.formGroupLogin= this.createLoginForm();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.changeheight();
  }

  changeheight(){
    const changeHeightSlide = Array.from(document.getElementsByClassName('carousel-size') as HTMLCollectionOf<HTMLElement>)
    //const changeHeightSlide = document.querySelectorAll<HTMLElement>('.carousel-size');
    changeHeightSlide.forEach((element) => {
        //element.outerHTML = '<div class="good-day-today" style="width: 0px;"></div>'; // Please note that this line works fine!
        element.style.height = `${window.innerHeight}px`;
    });
  }

  createLoginForm(): FormGroup {
      return this._formBuilder.group({
        username: ["", [Validators.required, Validators.email]],
        password: ["", [Validators.required]],
        rememberme: [true],
      });
  }

  // On submit button click
  onLogin(event: any) {
    const request = this.formGroupLogin.getRawValue();
    request.password = Utils.encrypt(request.password)
    this.genericLoadingService.show()
    this._securityService
      .login(request)
      .subscribe(
        (res: any) => {
          this.genericLoadingService.hide();
        },
        (res: any) => {
          this.messageError(res.error.key??'');
          this.genericLoadingService.hide();
        }
      );
  }

  messageError(code) {
    switch (code) {
      case 'INVALID_USER_NOT_FOUND':
        this._sweetAlertService.showToastInfo(
          "El usuario no corresponde a una cuenta inscrita"
        );
      break;
      case 'INVALID_EMAIL_NOT_CONFIRMED':
        this._sweetAlertService.showToastInfo(
          "El email aun no se encuentra confirmado, contacte a su administrador"
        );
      break;
      case 'INVALID_ROL_USER':
        this._sweetAlertService.showToastInfo(
          "Lo sentimos, tu cuenta no tiene un rol admitido"
        );
      case 'EMAIL_OR_PASWORD_INVALID':
        this._sweetAlertService.showToastInfo(
          "Usuario o contraseña incorrectos"
        );
      break;
      default:
        this._sweetAlertService.showToastInfo(
          "Ha ocurrido un error desconocido"
        );
        break;
    }
  }
}

