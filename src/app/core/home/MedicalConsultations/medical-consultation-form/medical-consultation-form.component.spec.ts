import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalConsultationFormComponent } from './medical-consultation-form.component';

describe('MedicalConsultationFormComponent', () => {
  let component: MedicalConsultationFormComponent;
  let fixture: ComponentFixture<MedicalConsultationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalConsultationFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalConsultationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
