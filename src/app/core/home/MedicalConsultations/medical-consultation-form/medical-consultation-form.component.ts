import { ChangeDetectorRef, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MedicalConsultation, Patient } from 'app/shared/api/pediakids.model';
import { PedikidsService } from 'app/shared/api/pediakids.service';
import { InputType } from 'app/shared/components/generic-input/generic-input.component';
import { SelectType } from 'app/shared/components/generic-select/generic-select.component';
import { SweetAlertService } from 'app/shared/services/sweet-alert.service';
import { CrudOperation, Utils } from 'app/shared/utils/utils.resource';
import { Subscription } from 'rxjs/internal/Subscription';
import { Location } from "@angular/common";
import { PatientListService } from '../../patient/patient-list/patient-list.service';
import { MedicalConsultationService } from '../medical-consultation-list/medical-consultation.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { PagedData } from 'app/shared/components/generic-datatable/model/paged-data';
import { GenericModalService } from 'app/shared/components/generic-modal/generic-modal.service';
import { PrintOptionsComponent } from '../print-options/print-options.component';

@Component({
  selector: 'app-medical-consultation-form',
  templateUrl: './medical-consultation-form.component.html',
  styleUrls: ['./medical-consultation-form.component.scss']
})
export class MedicalConsultationFormComponent implements OnInit {
  _subscriptionData: Subscription;
  _accionCrudValue=CrudOperation.ADD_RECORD;
  medicalConsultation:MedicalConsultation;
  medicalConsultationForm: FormGroup;
  selectType= SelectType
  inputType = InputType;
  patientId:number;
  patientInfo;
  patientAge;
  antecedenteConsultaAnterior:string="";
  valorSignosVitales:string="Normal";
  //Suscribes
  public _unsubscribeAll: Subject<any>;
  constructor(
    private _cd:ChangeDetectorRef,
    private sweetAlertService:SweetAlertService,
    private formBuilder: FormBuilder,
    private pediakidsService:PedikidsService,
    private route: ActivatedRoute, private location: Location,
    private _patientListService: PatientListService,
    private _medicalConsultationService: MedicalConsultationService,
    private _genericModalService:GenericModalService,

  ) {
    this._unsubscribeAll= new Subject()
   }


  ngOnInit(): void {
    this._subscriptionData = this.route.params.subscribe((parameters) => {
      this.patientId = parameters.data;
      this.getPatientById();
      if(parameters.consultationId != "null")
      {
        this.getListMedicalConsultationById(parameters.consultationId);
      }
    });
  }
  ngAfterViewInit(): void {}

  ngOnDestroy(): void {
    this._subscriptionData.unsubscribe();
  }
  getListMedicalConsultationById(id:number){
    this._medicalConsultationService.page.filterQuery= `Id eq ${id}`
    this._medicalConsultationService.onChangeListMedicalConsultation
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((result: PagedData<any>) => {
      this.antecedenteConsultaAnterior = result.data[0].Antecedentes;
    });
    this._medicalConsultationService.getMedicalConsultation()
  }

  getPatientById(){
    this._patientListService.page.expand= `Genero($select=Name)`
    this.pediakidsService.getPatiendById(this._patientListService.page.expand,this.patientId)
    .subscribe((res:any)=>{
      this.patientInfo = res;
      this.patientAge = this._medicalConsultationService.getAge(res.FechaNacimiento);
      this.medicalConsultation=<MedicalConsultation>{
        PatientId:Number(this.patientInfo.Id),
        TipoIdentificacionId:Number(this.patientInfo.TipoIdentificacionId),
        NumeroIdentificacion:this.patientInfo.NumeroIdentificacion
      };
      this.medicalConsultationForm= this.returnMedicalConsultationForm();
      Utils.markFormGroupTouched(this.medicalConsultationForm);
    },(error:any)=>{
      this.sweetAlertService.showToastError("No fue posible consultar al paciente, intente mas tarde" + error);
    })
  }

  returnMedicalConsultationForm(): FormGroup {
    return this.formBuilder.group({
      Id: [this.medicalConsultation.Id?this.medicalConsultation.Id:0],
      Motivo: [this.medicalConsultation.Motivo, [Validators.required]],
      EvolucionEnfermedad: [this.medicalConsultation.EvolucionEnfermedad, [Validators.required]],
      Antecedentes: [this.medicalConsultation.Antecedentes, [Validators.required]],
      Peso: [this.medicalConsultation.Peso, [Validators.required,Validators.maxLength(20)]],
      Talla: [this.medicalConsultation.Talla, [Validators.required,Validators.maxLength(3)]],
      PerimetoCefalico: [this.medicalConsultation.PerimetoCefalico, [Validators.required]],
      FrecuenciaCardiaca: [this.medicalConsultation.FrecuenciaCardiaca, [Validators.required,Validators.maxLength(3)]],
      FrecuenciaResiratorio: [this.medicalConsultation.FrecuenciaResiratorio, [Validators.required, Validators.maxLength(2)]],
      Temperatura: [this.medicalConsultation.Temperatura, [Validators.required, Validators.maxLength(4)]],
      Saturacion: [this.medicalConsultation.Saturacion, [Validators.maxLength(3)]],
      TencionArterial: [this.medicalConsultation.TencionArterial, [Validators.maxLength(7)]],
      Cabeza: [this.medicalConsultation.Cabeza?this.medicalConsultation.Cabeza: null, [Validators.maxLength(500)]],
      ORL: [this.medicalConsultation.ORL?this.medicalConsultation.ORL: null, [Validators.maxLength(500)]],
      Cuello: [this.medicalConsultation.Cuello?this.medicalConsultation.Cuello: null, [Validators.maxLength(500)]],
      Torax: [this.medicalConsultation.Torax?this.medicalConsultation.Torax: null, [Validators.maxLength(500)]],
      Abdomen: [this.medicalConsultation.Abdomen?this.medicalConsultation.Abdomen: null, [Validators.maxLength(500)]],
      Genitales: [this.medicalConsultation.Genitales?this.medicalConsultation.Genitales: null, [Validators.maxLength(500)]],
      Extremidades: [this.medicalConsultation.Extremidades?this.medicalConsultation.Extremidades: null, [Validators.maxLength(500)]],
      Piel: [this.medicalConsultation.Piel?this.medicalConsultation.Piel: null, [Validators.maxLength(500)]],
      Neurologico: [this.medicalConsultation.Neurologico?this.medicalConsultation.Neurologico: null, [Validators.maxLength(500)]],
      Diagnostico: [this.medicalConsultation.Diagnostico, [Validators.required]],
      Tratamiento: [this.medicalConsultation.Tratamiento, null],
      Paraclinicos: [this.medicalConsultation.Paraclinicos, null],
      ParaclinicosOpcional: [this.medicalConsultation.ParaclinicosOpcional, null],
      PatientId: [this.medicalConsultation.PatientId],
      TipoIdentificacionId: [this.medicalConsultation.TipoIdentificacionId],
      NumeroIdentificacion: [this.medicalConsultation.NumeroIdentificacion],
    })
  }

  saveMedicalConsultation(medicalConsultation: FormGroup){
    let event=medicalConsultation.getRawValue();
    if(medicalConsultation.status === "INVALID")
    {
      this.medicalConsultationForm= this.returnMedicalConsultationForm();
      Utils.markFormGroupTouched(this.medicalConsultationForm);
      return this.sweetAlertService.showToastError("Llene los campos de manera correcta");
    }
    if(this._accionCrudValue===CrudOperation.ADD_RECORD)
    {
      this.pediakidsService.createMedicalConsultation(null,event).subscribe((res:any)=>{
        this._genericModalService.openModal(PrintOptionsComponent,{medicalConsultation:res,patientAge:this.patientAge,patientinfo:this.patientInfo},null).then(()=>{},()=>{});
        this.sweetAlertService.showToastSucces(null);
        this.eventClickBack();
      },(error:any)=>{
        this.sweetAlertService.showToastError(error);
      })
    }
  }

  eventClickBack(){
    this.location.back();
  }

}
