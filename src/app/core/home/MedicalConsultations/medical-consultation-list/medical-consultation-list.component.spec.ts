import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalConsultationListComponent } from './medical-consultation-list.component';

describe('MedicalConsultationListComponent', () => {
  let component: MedicalConsultationListComponent;
  let fixture: ComponentFixture<MedicalConsultationListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalConsultationListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalConsultationListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
