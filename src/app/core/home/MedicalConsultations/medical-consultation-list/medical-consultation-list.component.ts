import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PedikidsService } from 'app/shared/api/pediakids.service';
import { Column } from 'app/shared/components/generic-datatable/model/columns.model';
import { PagedData } from 'app/shared/components/generic-datatable/model/paged-data';
import { BasePagination } from 'app/shared/odata/base-pagination';
import { SweetAlertService } from 'app/shared/services/sweet-alert.service';
import { CrudOperation } from 'app/shared/utils/utils.resource';
import { Subject } from 'rxjs/internal/Subject';
import { PatientListService } from '../../patient/patient-list/patient-list.service';
import { takeUntil } from 'rxjs/operators';
import { MedicalConsultationService } from './medical-consultation.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { Patient } from 'app/shared/api/pediakids.model';
import { Location } from "@angular/common";
import * as moment from 'moment';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { GenericModalService } from 'app/shared/components/generic-modal/generic-modal.service';
import { PrintOptionsComponent } from '../print-options/print-options.component';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-medical-consultation-list',
  templateUrl: './medical-consultation-list.component.html',
  styleUrls: ['./medical-consultation-list.component.scss']
})
export class MedicalConsultationListComponent extends BasePagination implements OnInit, AfterViewInit, OnDestroy {

  _subscriptionData: Subscription;
  _accionCrudValue=CrudOperation.ADD_RECORD;
  patient:Patient;
  idPatient:number;
  dataInformatinPatien;
  agePatient;
  //Datatble
  @ViewChild('cellTemplateOpcionesTable')
  public cellTemplateOpcionesTable: TemplateRef<any>;
  public columns:Array<Column> = [];
  pageDataPatients = new PagedData<any>();

  //Suscribes
  public _unsubscribeAll: Subject<any>;
  constructor(
    private _router: Router,
    private _pediakidsService:PedikidsService,
    private _medicalConsultationService: MedicalConsultationService,
    private _patientListService: PatientListService,
    private _route: ActivatedRoute,
    private _cd:ChangeDetectorRef,
    private notify: SweetAlertService,
    private route: ActivatedRoute, private location: Location,
    private _genericModalService:GenericModalService,
    ) {
      super()
      this._unsubscribeAll= new Subject()
    }

    ngOnInit(): void {
      this._subscriptionData = this.route.params.subscribe((parameters) => {
        this.idPatient = parameters.data;
        this.getPatientById();
        this.createColumns();
      });
    }

  ngAfterViewInit(): void {
    this.load();
  }

  ngOnDestroy(): void {
    if(this._unsubscribeAll){
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
      this._unsubscribeAll.unsubscribe();
    }
  }

  load(){
    this.createColumns();
    this.getListMedicalConsultationById();
  }

  datePipe(value: any, ...args: any[]) {
    return moment.utc(value).utcOffset(0).format('YYYY/MM/DD hh:mm:ss A');
  }

  createColumns(){
    this.columns=[
      { name: 'Consecutivo',prop:'Id' , sortable:true },
      { name: 'Fecha Consulta',prop:'CreatedDate', sortable:true, pipe: { transform: this.datePipe } },
      { name:"Opciones",prop:"Opciones",cellTemplate:this.cellTemplateOpcionesTable, sortable:false, width:130, maxWidth:130}
    ]
  }

getPatientById(){
  this._patientListService.page.expand= `Genero($select=Name)`
  this._pediakidsService.getPatiendById(this._patientListService.page.expand,this.idPatient)
  .subscribe((res:any)=>{
    this.dataInformatinPatien = res;
    this.agePatient = this._medicalConsultationService.getAge(res.FechaNacimiento);
  },(error:any)=>{
    this.notify.showToastError("No fue posible consultar al paciente, intente mas tarde");
  })
}

  getListMedicalConsultationById(){
    this._medicalConsultationService.page.sortBy= `CreatedDate`
    this._medicalConsultationService.page.sortDir= `desc`
    this._medicalConsultationService.page.filterQuery= `PatientId eq ${this.idPatient}`
    this._medicalConsultationService.onChangeListMedicalConsultation
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((result: PagedData<any>) => {
      this.pageDataPatients= result;
      this._cd.detectChanges();
    });
    this._medicalConsultationService.getMedicalConsultation()
  }

  filterOdataEvent($event){
    this._medicalConsultationService.filterOdataEvent($event)
  }

  nexPageEvent($event){
    this._medicalConsultationService.nexPageEvent($event)
  }

  sortEvent($event){
    this._medicalConsultationService.sortEvent($event)
  }

  limitPageEvent($event){
    this._medicalConsultationService.limitPageEvent($event)
  }


async viewHistoryEventClick(data:any){
  this._genericModalService.openModal(PrintOptionsComponent,{medicalConsultation:data,patientAge:this.agePatient,patientinfo:this.dataInformatinPatien},null).then(()=>{},()=>{});
}

  createMedicalConsultation(){
    let medicalConsultationId;
    if(this.pageDataPatients.data.length > 0)
    {
      medicalConsultationId = this.pageDataPatients.data.map(x=>x.Id).reduce((n,m) => Math.max(n,m));
    }
    this._router.navigate(['paediakids/core/medical-consultation/medical-consultation-form',{accion:CrudOperation.ADD_RECORD,data:this.dataInformatinPatien.Id,consultationId:medicalConsultationId}]);
  }

  eventClickBack() {
    this.location.back();
  }

}
