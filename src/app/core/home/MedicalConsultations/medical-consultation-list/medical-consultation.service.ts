import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { PedikidsService } from "app/shared/api/pediakids.service";
import { PagedData } from "app/shared/components/generic-datatable/model/paged-data";
import { BasePagination, IListenPagination } from "app/shared/odata/base-pagination";
import { OdataPaginationService } from "app/shared/odata/odata-pagination.service";
import { Subject } from "rxjs";
import { map } from "rxjs/operators";
import * as moment from 'moment';

@Injectable({
  providedIn: "root",
})

export class MedicalConsultationService extends BasePagination implements IListenPagination {
  onChangeListMedicalConsultation= new Subject<PagedData<any>>();
  constructor(
    private _pediakitService: PedikidsService,
    private odataPaginationService:OdataPaginationService,
    private http: HttpClient) {
      super()
  }

  /**
  * Populate the table with new data based on the page number
  * @param page The page to select
  */
  getMedicalConsultation() {
    this._pediakitService.getMedicalConsultations(
      this.page.getFilterQueryComplete,
        this.page.itemsPerPage,
        this.page.currentPage,
        `${this.page.sortBy} ${this.page.sortDir}`,
        this.page.itemsPerPage != null && this.page.currentPage != null,
        this.page.expand,
        null,
        this.page.select
      ).pipe(
        map((d) =>this.odataPaginationService.getPagedData(d, this.page, this.page))
      ).subscribe((pagedData) => {
        this.onChangeListMedicalConsultation.next(pagedData);
      });
  }

  onNexPageEvent(nextPage: boolean): void {
    if(nextPage){
      this.getMedicalConsultation()
    }
  }

  onSortEvent(): void {
    this.getMedicalConsultation()
  }

  onLimitPageEvent(): void {
    this.getMedicalConsultation()
  }

  onFilterOdataEvent(): void {
    this.getMedicalConsultation()
  }

  getAge(fechaNacimiento:string):string{
    var a = moment(new Date());
    var b = moment(new Date(fechaNacimiento));
    a.add(1, 'd');
    b.diff(a, 'days');
    b.diff(a, 'months');
    b.diff(a, 'years');
    var years = a.diff(b, 'year');
    b.add(years, 'years');
    var months = a.diff(b, 'months');
    b.add(months, 'months');
    var days = a.diff(b, 'days');
    return years + " año(s) " + "  " + months + " Mes(es) " + "  " + days + " día(s) ";
  }

  async  getBase64ImageFromUrl(imageUrl) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();
  
    return new Promise((resolve, reject) => {
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);
  
      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }
  
}
