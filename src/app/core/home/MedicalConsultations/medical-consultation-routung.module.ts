import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'app/shared/auth/auth.guard';
import { MedicalConsultationFormComponent } from './medical-consultation-form/medical-consultation-form.component';
import { MedicalConsultationListComponent } from './medical-consultation-list/medical-consultation-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        canActivate: [AuthGuard],
        component: MedicalConsultationListComponent,
        data: {
          title: 'Page',
          roles: ['Authenticated'],
        }
      },
      {
        path: 'app-medical-consultation-list',
        canActivate: [AuthGuard],
        component: MedicalConsultationListComponent,
        data: {
          title: 'Page',
          roles: ['Authenticated'],
        }
      },
      {
        path: 'medical-consultation-form',
        canActivate: [AuthGuard],
        component: MedicalConsultationFormComponent,
        data: {
          title: 'Page',
          roles: ['Authenticated'],
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultationMedicalRoutungModule { }
