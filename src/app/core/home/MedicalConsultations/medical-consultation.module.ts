import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MedicalConsultationListComponent } from './medical-consultation-list/medical-consultation-list.component';
import { ConsultationMedicalRoutungModule } from './medical-consultation-routung.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PatientRoutingModule } from '../patient/patient-routing.module';
import { GenericInputModule } from 'app/shared/components/generic-input/generic-input.module';
import { GenericDatatableModule } from 'app/shared/components/generic-datatable/generic-datatable.module';
import { GenericOptionsModule } from 'app/shared/components/generic-options/generic-options.module';
import { GenericSelectModule } from 'app/shared/components/generic-select/generic-select.module';
import { GenericInputDatepickerModule } from 'app/shared/components/generic-input-datepicker/generic-input-datepicker.module';
import { MedicalConsultationFormComponent } from './medical-consultation-form/medical-consultation-form.component';
import { GenericOptionsMedicalConsultationModule } from 'app/shared/components/generic-options-medical-consultation/generic-options-medical-consultation.module';
import { GenericModalModule } from 'app/shared/components/generic-modal/generic-modal.module';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { PrintOptionsComponent } from './print-options/print-options.component'
const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    MedicalConsultationListComponent,
    MedicalConsultationFormComponent,
    PrintOptionsComponent,
  ],  
  imports: [
    CommonModule,
    ConsultationMedicalRoutungModule,
    FormsModule ,
    ReactiveFormsModule,
    PatientRoutingModule,
    GenericInputModule,
    GenericDatatableModule,
    GenericOptionsModule,
    GenericSelectModule,
    GenericInputDatepickerModule,
    GenericOptionsMedicalConsultationModule,
    GenericModalModule,
    NgxMaskModule.forRoot(maskConfig),
    GenericModalModule
  ]
})
export class MedicalConsultatioModule { }
