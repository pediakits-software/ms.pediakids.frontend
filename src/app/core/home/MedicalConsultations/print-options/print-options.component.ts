import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";
import { ActivatedRoute } from '@angular/router';
import { GenericModalService } from 'app/shared/components/generic-modal/generic-modal.service';
import * as moment from 'moment';
import * as pdfMake from "pdfmake/build/pdfmake";
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
(<any>pdfMake).vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-print-options',
  templateUrl: './print-options.component.html',
  styleUrls: ['./print-options.component.scss']
})
export class PrintOptionsComponent implements OnInit {
  title:any = "Selecione las opciónes que desea imprimir";
  historiaClinica=false;
  tratamiento=true;
  paraclinicos=false;
  dataMedicalConsultation:any;
  logoUrl:string = "./assets/img/recetario.png";
  constructor(
    private _genericModalService:GenericModalService,
    private route: ActivatedRoute, private location: Location,
    ) { }
  
  ngOnInit(): void {
    this.dataMedicalConsultation =  this._genericModalService.data;
  }

  async receiveMessage($event) {
    if(this.historiaClinica)
    {      
      let medicalConsultationPrint = await this.MedicalConsultationPrint();
      pdfMake.createPdf(medicalConsultationPrint).print();
    }
    if(this.tratamiento)
    {      
      let treatmentPrint = await this.treatment();
      pdfMake.createPdf(treatmentPrint).print();
    }
    if(this.paraclinicos)
    {
      let paraclinicalPrint = await this.paraclinical();
      pdfMake.createPdf(paraclinicalPrint).print()
    }
  }
  
  async paraclinical(){
    let image;
     await this.getBase64ImageFromUrl(this.logoUrl)
    .then(result => image = result)
    .catch(err => console.error(err)); 

    const pdfDefinition: any = {
      pageSize: 'A5',
      pageMargins: [20, 180, 20, 40],
      header: function() {
        return {
          stack: [
            {
              image: image,
              fit: [400, 2000],
              alignment: 'center'
            },
            {
              columns: [
                {
                  width: '20%',
                  text: [
                    { text: 'P: ', style: 'initialText' },
                    { text: this.dataMedicalConsultation.medicalConsultation.Peso, style: 'finalText' }
                  ]
                },
                {
                  width: '20%',
                  text: [
                    { text: 'T: ', style: 'initialText' },
                    { text: this.dataMedicalConsultation.medicalConsultation.Talla + ' cm', style: 'finalText' }
                  ]
                },
                {
                  width: '20%',
                  text: [
                    { text: 'PC: ', style: 'initialText' },
                    { text: this.dataMedicalConsultation.medicalConsultation.PerimetoCefalico, style: 'finalText' }
                  ]
                },
                {
                  width: '40%',
                  text: [
                    { text: moment.utc(this.dataMedicalConsultation.medicalConsultation.CreatedDate).utcOffset(0).format('YYYY/MM/DD hh:mm:ss A'), style: 'finalText' },
                    '\n',
                  ]
                }
              ],
              margin: [0, 0] 
            },
            {
              columns: [
                {
                  text: [
                    { text: 'Nombres y Apellidos: ', style: 'initialText' },
                    { text: this.dataMedicalConsultation.patientinfo.Nombres + ' ' + this.dataMedicalConsultation.patientinfo.Apellidos, style: 'finalText' },
                    '\n',
                    { text: 'Identificación: ', style: 'initialText' },
                    { text: this.dataMedicalConsultation.patientinfo.NumeroIdentificacion, style: 'finalText' },
                    '\n',
                    { text: 'Edad: ', style: 'initialText' },
                    { text: this.dataMedicalConsultation.patientAge, style: 'finalText' }
                  ]
                }
              ]
            },
            { text: '\n' } 
          ],
          margin: [20, 0, 20, 0]
        };
      }.bind(this),
    
      content: [
        {
          text: this.dataMedicalConsultation.medicalConsultation.Paraclinicos,
          style: 'finalText',
        },
        {
          text: this.dataMedicalConsultation.medicalConsultation.ParaclinicosOpcional,
          style: 'finalText',
          pageBreak: 'before'
        },
      ],
      
      footer: function(currentPage, pageCount) 
      {
         return [
          {
          text: 'Calle 3 sur n 4-110 Pitalito-Huila al frente del antiguo Hotel Timanco',
          style: 'footer',
          alignment: 'center'
         },
         {
          text: currentPage.toString() + ' pagina de ' + pageCount,
          alignment: 'center',
          fontSize: 9,
          color: '#2e3092' 
         },
         {
          text: 'Fecha de Impresión ' + moment(new Date()).format('YYYY/MM/DD hh:mm:ss a'),
          alignment: 'center',
          fontSize: 9,
          color: '#2e3092' 
         },
        ]
         
      },
      styles: {
        initialText:{
          bold:true, 
          fontSize:10
        },
        finalText:{
          fontSize:10, 
          bold:false,
          alignment: 'justify'
        },
        footer: {
          fontSize: 11,
          bold: true,
          alignment: 'justify',
          color: '#2e3092' 
        },
        contentHeader:{
          fontSize: 14,
          bold: true,
        },
        contentHeaderNext:{
          fontSize: 14,
          bold: true,
          margin: [0, 13, 0, 0],
        }
      }
    };
    return pdfDefinition;
  }

  async treatment(){
    let image;
     await this.getBase64ImageFromUrl(this.logoUrl)
    .then(result => image = result)
    .catch(err => console.error(err)); 

    const pdfDefinition:any={
      pageSize: 'A5',
      pageMargins: [ 20, 120, 20, 40 ],
      header: function(currentPage, pageCount, pageSize) {
        return [
          {
            image: image,
            fit: [400, 2000],
            alignment: 'center'
          }
        ]
      },
      content:[
        {
          columns:[
            {
              width: '20%',
              text:[
                {text:'P: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.Peso, style: 'finalText' }
              ]
            },
            {
              width: '20%',
              text:[
                {text:'T: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.Talla + ' cm', style: 'finalText' }
              ]
            },
            {
              width: '20%',
              text:[
                {text:'PC: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.PerimetoCefalico, style: 'finalText' }
              ]
            },
            {
              width: '40%',
              text:[
                {text: moment.utc(this.dataMedicalConsultation.medicalConsultation.CreatedDate).utcOffset(0).format('YYYY/MM/DD hh:mm:ss A'), style: 'finalText' },
                '\n',
              ]
            }
            
          ]
        },
        {
          columns: [
            {
              text: [
                {text:'Nombres y Apellidos: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientinfo.Nombres +' '+ this.dataMedicalConsultation.patientinfo.Apellidos, style: 'finalText' },
                '\n',
                {text:'Identificación: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientinfo.NumeroIdentificacion, style: 'finalText' },
                '\n',
                {text:'Edad: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientAge, style: 'finalText' }
                
              ]
            }
          ]
        },
        {
          text: [
            '\n',
            {text: this.dataMedicalConsultation.medicalConsultation.Tratamiento, style: 'finalText' }
          ] 
        },
        
      ],
      footer: function(currentPage, pageCount) 
      {
         return [
          {
          text: 'Calle 3 sur n 4-110 Pitalito-Huila al frente del antiguo Hotel Timanco',
          style: 'footer',
          alignment: 'center'
         },
         {
          text: currentPage.toString() + ' pagina de ' + pageCount,
          alignment: 'center',
          fontSize: 9,
          color: '#2e3092' 
         },
         {
          text: 'Fecha de Impresión ' + moment(new Date()).format('YYYY/MM/DD hh:mm:ss a'),
          alignment: 'center',
          fontSize: 9,
          color: '#2e3092' 
         },
        ]
         
      },
      styles: {
        initialText:{
          bold:true, 
          fontSize:10
        },
        finalText:{
          fontSize:10, 
          bold:false,
          alignment: 'justify'
        },
        footer: {
          fontSize: 11,
          bold: true,
          alignment: 'justify',
          color: '#2e3092' 
        },
        contentHeader:{
          //margin: [0, 120, 0, 0],
          fontSize: 14,
          bold: true,
        },
        contentHeaderNext:{
          fontSize: 14,
          bold: true,
          margin: [0, 13, 0, 0],
        }
      }
    }
    
    return pdfDefinition;
  }

  async  getBase64ImageFromUrl(imageUrl) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();
  
    return new Promise((resolve, reject) => {
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);
  
      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }

  async MedicalConsultationPrint(){
    let image;
     await this.getBase64ImageFromUrl(this.logoUrl)
    .then(result => image = result)
    .catch(err => console.error(err)); 

    const pdfDefinition:any={
      pageMargins: [ 40, 155, 40, 60 ],
      header: function(currentPage, pageCount, pageSize) {
        return [
          {
            image: image,
            fit: [500, 3000],
            alignment: 'center'
          }
        ]
      },
      content:[
        {text: 'Información básica del paciente:',style: 'contentHeader'},
        {
          columns: [
            {
              text: [
                {text:'Nombres y Apellidos: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientinfo.Nombres +' '+ this.dataMedicalConsultation.patientinfo.Apellidos, style: 'finalText' },
                '\n',
                {text:'Identificación: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientinfo.NumeroIdentificacion, style: 'finalText' },
                '\n',
                {text:'Genero: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientinfo.Genero.Name, style: 'finalText' },
                '\n',
                {text:'Edad: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientAge, style: 'finalText' },
              ]
            },
            {
              text: [
                {text:'Fecha Nacimiento: ', style: 'initialText'},
                {text: moment(this.dataMedicalConsultation.patientinfo.FechaNacimiento).format('YYYY/MM/DD'), style: 'finalText' },
                '\n',
                {text:'Telefono: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientinfo.Telefono, style: 'finalText' },
                '\n',
                {text:'Dirección: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.patientinfo.Direccion, style: 'finalText' },
              ] 
            }
          ]
        },
        {text: 'Datos de la cosulta:',style: 'contentHeaderNext'},
        {
          text: [
            {text:'Fecha de Atención: ', style: 'initialText'},
            {text: moment.utc(this.dataMedicalConsultation.medicalConsultation.CreatedDate).utcOffset(0).format('YYYY/MM/DD hh:mm:ss A'), style: 'finalText' },
            '\n',
            '\n',
            {text:'Motivo de atención: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Motivo, style: 'finalText' },
            '\n',
            '\n',
            {text:'Antecedentes: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Antecedentes, style: 'finalText' },
            '\n',
            '\n',
            {text:'Evolución Enfermedad: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.EvolucionEnfermedad, style: 'finalText' },
          ] 
        },
        {text: 'Examen Fisico:',style: 'contentHeaderNext'},
        {
          columns:[
            {
              text:[
                {text:'Peso: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.Peso, style: 'finalText' }
              ]
            },
            {
              text:[
                {text:'Talla: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.Talla + ' cm', style: 'finalText' }
              ]
            },
            {
              text:[
                {text:'Perimetro Cefalico: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.PerimetoCefalico, style: 'finalText' }
              ]
            }
          ]
        },
        {text: 'Signos Vitales:',style: 'contentHeaderNext'},
        {
          columns:[
            {
              text:[
                {text:'Fecuencia Cardiaca: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.FrecuenciaCardiaca, style: 'finalText' }
              ]
            },
            {
              text:[
                {text:'Fecuencia Respiratoria: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.FrecuenciaResiratorio, style: 'finalText' }
              ]
            },
            {
              text:[
                {text:'Temperatura: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.Temperatura + '°', style: 'finalText' }
              ]
            }
          ]
        },
        {
          columns:[
            {
              text:[
                {text:'Saturación: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.Saturacion, style: 'finalText' }
              ]
            },
            {
              text:[
                {text:'Tencion Arterial: ', style: 'initialText'},
                {text: this.dataMedicalConsultation.medicalConsultation.TencionArterial, style: 'finalText' }
              ]
            },
            {
              //dejas vacia
            }
          ]
        },
        {
          text: [
            {text:'Cabeza: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Cabeza, style: 'finalText' },
            '\n',
            {text:'ORL: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.ORL, style: 'finalText' },
            '\n',
            {text:'Cuello: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Cuello, style: 'finalText' },
            '\n',
            {text:'Torax: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Torax, style: 'finalText' },
            '\n',
            {text:'Abdomen: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Abdomen, style: 'finalText' },
            '\n',
            {text:'Genitales: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Genitales, style: 'finalText' },
            '\n',
            {text:'Extremidades: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Extremidades, style: 'finalText' },
            '\n',
            {text:'Piel: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Piel, style: 'finalText' },
            '\n',
            {text:'Neurológico: ', style: 'initialText'},
            {text: this.dataMedicalConsultation.medicalConsultation.Neurologico, style: 'finalText' },
            '\n',
            '\n',
            {text:'Diagnóstico: ', style: 'contentHeaderNext'},
            '\n',
            {text: this.dataMedicalConsultation.medicalConsultation.Diagnostico, style: 'finalText' },
            '\n',
            '\n',
            {text: 'Tratamiento:',style: 'contentHeaderNext'},
            '\n',
            {text: this.dataMedicalConsultation.medicalConsultation.Tratamiento, style: 'finalText' },
            '\n',
            '\n',
            {text:'Paraclínicos: ', style: 'contentHeaderNext'},
            '\n',
            {text: this.dataMedicalConsultation.medicalConsultation.Paraclinicos, style: 'finalText' },
            '\n',
            '\n',
            {text:'Paraclínicos 2: ', style: 'contentHeaderNext'},
            '\n',
            {text: this.dataMedicalConsultation.medicalConsultation.ParaclinicosOpcional, style: 'finalText' },
          ] 
        },
        
  
      ],
      footer: function(currentPage, pageCount) 
      {
         return [
          {
          text: 'Calle 3 sur n 4-110 Pitalito-Huila al frente del antiguo Hotel Timanco',
          style: 'footer',
          alignment: 'center'
         },
         {
          text: currentPage.toString() + ' pagina de ' + pageCount,
          alignment: 'center',
          fontSize: 9,
          color: '#2e3092' 
         },
         {
          text: 'Fecha de Impresión ' + moment(new Date()).format('YYYY/MM/DD hh:mm:ss a'),
          alignment: 'center',
          fontSize: 9,
          color: '#2e3092' 
         },
        ]
         
      },
      styles: {
        initialText:{
          bold:true, 
          fontSize:10
        },
        finalText:{
          fontSize:10, 
          bold:false,
          alignment: 'justify'
        },
        footer: {
          fontSize: 12,
          bold: true,
          alignment: 'justify',
          color: '#2e3092' 
        },
        contentHeader:{
          //margin: [0, 120, 0, 0],
          fontSize: 14,
          bold: true,
        },
        contentHeaderNext:{
          fontSize: 14,
          bold: true,
          margin: [0, 13, 0, 0],
        }
      }
    }
    return pdfDefinition;
  }

  eventClickBack(){
    this.location.back();
  }
}
