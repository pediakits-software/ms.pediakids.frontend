import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'patient',
    loadChildren: () => import('./patient/patient.module').then( (modulo) => modulo.PatientModule ),
    //canLoad: [ LoginGuard ]
  },
  {
    path: 'reports',
    loadChildren: () => import('./reports/reports.module').then( (modulo) => modulo.ReportsModule ),
    //canLoad: [ LoginGuard ]
  },
  {
    path: 'medical-consultation',
    loadChildren: () => import('./MedicalConsultations/medical-consultation.module').then( (modulo) => modulo.MedicalConsultatioModule ),
    //canLoad: [ LoginGuard ]
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( (modulo) => modulo.ProfileModule ),
    //canLoad: [ LoginGuard ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
