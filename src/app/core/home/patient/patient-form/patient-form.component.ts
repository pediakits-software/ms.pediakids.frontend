import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { CrudOperation, ITEM_TYPE, Utils } from "app/shared/utils/utils.resource";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Patient } from "app/shared/api/pediakids.model";
import { InputType } from "app/shared/components/generic-input/generic-input.component";
import { ItemSelect, SelectType } from "app/shared/components/generic-select/generic-select.component";
import { PedikidsService } from "app/shared/api/pediakids.service";
import { CustomValidators } from "app/shared/validators/custom-validators";
import { SweetAlertService } from "app/shared/services/sweet-alert.service";

@Component({
  selector: "app-patient-form",
  templateUrl: "./patient-form.component.html",
  styleUrls: ["./patient-form.component.scss"],
})
export class PatientFormComponent implements OnInit, OnDestroy, AfterViewInit {
  _subscriptionData: Subscription;
  _accionCrudValue=CrudOperation.ADD_RECORD;
  patient:Patient;
  patientForm: FormGroup;
  selectType= SelectType
  inputType = InputType;
  defaultTipoIdentificaciones:number;
  defaultGeneros:number;
  //List
  listDataGeneros:ItemSelect[]=[]
  listDataTipoIdentificaciones:ItemSelect[]=[]
  constructor(
    private _cd:ChangeDetectorRef,
    private sweetAlertService:SweetAlertService,
    private formBuilder: FormBuilder,
    private pediakidsService:PedikidsService,
    private route: ActivatedRoute, private location: Location
    ) {}

  ngAfterViewInit(): void {}
  ngOnDestroy(): void {
    this._subscriptionData.unsubscribe();
  }

  ngOnInit(): void {
    this._subscriptionData = this.route.params.subscribe((parameters) => {
      this._accionCrudValue= parameters.accion;
      if(this._accionCrudValue===CrudOperation.EDIT_RECORD){
        this.patient=<Patient>{Id:Number(parameters.id)};
        this.findPatient();
      }else{
        this.patient=<Patient>{};
        this.patientForm= this.returnPatientForm();
        Utils.markFormGroupTouched(this.patientForm)
      }
      this.load();
    });
  }

  returnPatientForm(): FormGroup {
    return this.formBuilder.group({
      Id: [this.patient.Id?this.patient.Id:0],
      Nombres: [this.patient.Nombres, [Validators.required]],
      Apellidos: [this.patient.Apellidos, [Validators.required]],
      NumeroIdentificacion: [this.patient.NumeroIdentificacion, [Validators.required, Validators.maxLength(20), CustomValidators.validateOnlyNumber]],
      FechaNacimiento: [this.patient.FechaNacimiento?this.patient.FechaNacimiento: null, [Validators.required]],
      Direccion: [this.patient.Direccion],
      Telefono: [this.patient.Telefono, [CustomValidators.validateOnlyNumber]],
      TipoIdentificacionId: [this.patient.TipoIdentificacionId, [Validators.required]],
      GeneroId: [this.patient.GeneroId, [Validators.required]]
    })
  }

  findPatient(){
    this.pediakidsService.getPatiendById(null,this.patient.Id).subscribe((res: any) => {
      this.patient =  res;
      this.patientForm= this.returnPatientForm();
    })
  }

  load(){
    this.pediakidsService.getItemList(Utils.filterInTipos([ITEM_TYPE.GENERO,ITEM_TYPE.TIPO_IDENTIFICACION]), null, null, "Name asc", null, "TypeList($select=Name)", null, null).subscribe((res: any) => {
      this.listDataGeneros =  Utils.mapItemSelect(Utils.getParametrosByNombre(ITEM_TYPE.GENERO, res.value))
      this.listDataTipoIdentificaciones =  Utils.mapItemSelect(Utils.getParametrosByNombre(ITEM_TYPE.TIPO_IDENTIFICACION, res.value))
      this.defaultTipoIdentificaciones = this.listDataTipoIdentificaciones[0].value;
      this.defaultGeneros = this.listDataGeneros[0].value;
      this._cd.detectChanges();
    })
  }

  savePatient(patiend: FormGroup){
    let event=patiend.getRawValue();
    if(patiend.status === "INVALID")
    {
      return this.sweetAlertService.showToastError("Llene los campos de manera correcta");
    }
    if(this._accionCrudValue===CrudOperation.EDIT_RECORD){
      this.pediakidsService.updatePatient("Genero($select=Name)",this.patient.Id,event).subscribe((res:any)=>{
        this.sweetAlertService.showToastSucces(null);
        this.eventClickBack();
      },(error:any)=>{
        this.sweetAlertService.showToastError(null);
      })
    }else{
      this.pediakidsService.createPatient("Genero($select=Name)",event).subscribe((res:any)=>{
        this.sweetAlertService.showToastSucces(null);
        this.eventClickBack();
      },(error:any)=>{
        if(error.error){
          this.sweetAlertService.showToastError(error.error.message??'');
        } else {
          this.sweetAlertService.showToastError(null)
        }
      })
    }
  }

  eventClickBack() {
    this.location.back();
  }
}
