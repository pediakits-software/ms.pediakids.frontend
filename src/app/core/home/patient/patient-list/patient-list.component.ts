import { ChangeDetectorRef, Component, ViewChild, Input, SimpleChanges, TemplateRef, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { SweetAlertService } from 'app/shared/services/sweet-alert.service';
import { Column } from 'app/shared/components/generic-datatable/model/columns.model';
import { PagedData } from 'app/shared/components/generic-datatable/model/paged-data';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { PatientListService } from './patient-list.service';
import { BasePagination } from 'app/shared/odata/base-pagination';
import { CrudOperation } from 'app/shared/utils/utils.resource';
import { PedikidsService } from 'app/shared/api/pediakids.service';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent extends BasePagination implements OnInit, AfterViewInit, OnDestroy{

  //Datatble
  @ViewChild('cellTemplateOpcionesTable')
  public cellTemplateOpcionesTable: TemplateRef<any>;
  public columns:Array<Column> = [];
  pageDataPatients = new PagedData<any>();

  //Suscribes
  public _unsubscribeAll: Subject<any>;
  constructor(
    private _router: Router,
    private _pediakidsService:PedikidsService,
    private _patientListService: PatientListService,
    private _route: ActivatedRoute,
    private _cd:ChangeDetectorRef,
    private notify: SweetAlertService,) {
      super()

      this._unsubscribeAll= new Subject()
    }

    ngOnInit(): void {

    }
  ngAfterViewInit(): void {
    this.createColumns();
    this.getListPatients();
  }
  ngOnDestroy(): void {
    if(this._unsubscribeAll){
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
      this._unsubscribeAll.unsubscribe();
    }
  }

  createColumns(){
    this.columns=[
      { name: 'Paciente',prop:'Usuario', sortable:true },
      { name: 'Identificacion',prop:'NumeroIdentificacion', sortable:true },
      { name: 'Genero',prop:'Genero.Name', sortable:true },
      { name: 'Telefono',prop:'Telefono', sortable:true },
      { name:"Opciones",prop:"Opciones",cellTemplate:this.cellTemplateOpcionesTable, sortable:false, width:130, maxWidth:130}
    ]
    this._cd.detectChanges()

  }

  getListPatients(){
    this._patientListService.page.sortBy= `Nombres`
    this._patientListService.page.sortDir= `asc`
    this._patientListService.page.expand= `Genero($select=Name)`
    this._patientListService.onChangeListPatients
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((result: PagedData<any>) => {
      this.pageDataPatients= result;
      this._cd.detectChanges();
    });
    this._patientListService.getListPatients()
  }

  filterOdataEvent($event){
    this._patientListService.filterOdataEvent($event)
  }

  nexPageEvent($event){
    this._patientListService.nexPageEvent($event)
  }

  sortEvent($event){
    this._patientListService.sortEvent($event)
  }

  limitPageEvent($event){
    this._patientListService.limitPageEvent($event)
  }

  async deleteEventClick(data:any){
    let notify=await this.notify.showAlertConfirmation('¿Seguro que desea eliminar éste registro?','',
    'question','Eliminar',true,'Cancelar');
    if(notify){
      this._pediakidsService.deletePatient(data.Id).subscribe((res:any)=>{
        this.notify.showToastSucces(null);
      },(error:any)=>{
        this.notify.showToastError(null);
      })
    }
  }


viewHistoryEventClick(data:any){
  this._router.navigate(['paediakids/core/medical-consultation/app-medical-consultation-list',{accion:CrudOperation.VIEW_RECORD,data:data.Id}]);
}

editEventClick(data:any){
  this._router.navigate(['paediakids/core/patient/patient-form',{accion:CrudOperation.EDIT_RECORD, id:data.Id}]);

     /*  this._genericModalService.openModal(PatientFormComponent,
        {
          title:"Editar Tipo Riesgo",
          Patient:data,
          accion:CrudOperation.EDIT_RECORD,
          componenteId: this._parameters.componenteId,
          moduloId:  this._parameters.moduloId,
        }, true, ModalSize.LG).then(()=>{},()=>{}); */
  }

  crearPatient(){
    this._router.navigate(['paediakids/core/patient/patient-form',{accion:CrudOperation.ADD_RECORD}]);
    /* this._genericModalService.openModal(PatientFormComponent,
      {
        title:"Agregar Tipo Riesgo",
        accion:CrudOperation.ADD_RECORD,
        componenteId: this._parameters.componenteId,
        moduloId:  this._parameters.moduloId,
      }, true, ModalSize.LG).then(()=>{},()=>{}); */
  }
}
