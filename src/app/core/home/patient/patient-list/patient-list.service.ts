import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Patient } from "app/shared/api/pediakids.model";
import { PedikidsService } from "app/shared/api/pediakids.service";
import { PagedData } from "app/shared/components/generic-datatable/model/paged-data";
import { BasePagination, IListenPagination } from "app/shared/odata/base-pagination";
import { OdataPaginationService } from "app/shared/odata/odata-pagination.service";
import { Subject } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})

export class PatientListService extends BasePagination implements IListenPagination {
  onChangeListPatients= new Subject<PagedData<any>>();
  constructor(
    private _pediakitService: PedikidsService,
    private odataPaginationService:OdataPaginationService,
    private http: HttpClient) {
      super()
  }

  /**
  * Populate the table with new data based on the page number
  * @param page The page to select
  */

  getListPatients() {
    this._pediakitService.getPatients(
      this.page.getFilterQueryComplete,
        this.page.itemsPerPage,
        this.page.currentPage,
        `${this.page.sortBy} ${this.page.sortDir}`,
        this.page.itemsPerPage != null && this.page.currentPage != null,
        this.page.expand,
        null,
        this.page.select
      ).pipe(
        map((d) =>this.odataPaginationService.getPagedData(d, this.page, this.page))
      ).subscribe((pagedData) => {
        let items=[]
        if(pagedData.page.totalElements>0){
          items= pagedData.data.map(item => {
            return {
              Usuario: `${item.Nombres} ${item.Apellidos}`,
             ...item
            };
          });
        }
        pagedData.data= items
        this.onChangeListPatients.next(pagedData);
      });
  }

  onNexPageEvent(nextPage: boolean): void {
    if(nextPage){
      this.getListPatients()
    }
  }

  onSortEvent(): void {
    this.getListPatients()
  }

  onLimitPageEvent(): void {
    this.getListPatients()
  }

  onFilterOdataEvent(): void {
    this.getListPatients()
  }
}
