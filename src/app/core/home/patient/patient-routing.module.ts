import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/shared/auth/auth.guard';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { PatientListComponent } from './patient-list/patient-list.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        canActivate: [AuthGuard],
        component: PatientListComponent,
        data: {
          title: 'Page',
          roles: ['Authenticated'],
        }
      },
      {
        path: 'patient-form',
        canActivate: [AuthGuard],
        component: PatientFormComponent,
        data: {
          title: 'Page',
          roles: ['Authenticated'],
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PatientRoutingModule { }

