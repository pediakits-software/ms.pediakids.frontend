import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientFormComponent } from './patient-form/patient-form.component';
import { PatientRoutingModule } from './patient-routing.module';
import { GenericDatatableModule } from 'app/shared/components/generic-datatable/generic-datatable.module';
import { GenericOptionsModule } from 'app/shared/components/generic-options/generic-options.module';
import { GenericSelectModule } from 'app/shared/components/generic-select/generic-select.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenericInputModule } from 'app/shared/components/generic-input/generic-input.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { GenericInputDatepickerModule } from 'app/shared/components/generic-input-datepicker/generic-input-datepicker.module';
import { NgxMaskModule, IConfig } from 'ngx-mask'
const maskConfig: Partial<IConfig> = {
  validation: false,
};
@NgModule({
  declarations: [
    PatientListComponent,
    PatientFormComponent
  ],
  imports: [
    CommonModule,
    FormsModule ,
    ReactiveFormsModule,
    PatientRoutingModule,
    GenericInputModule,
    GenericDatatableModule, 
    GenericOptionsModule,
    GenericSelectModule,
    GenericInputDatepickerModule,
    NgxMaskModule.forRoot(maskConfig),
  ]
})
export class PatientModule { }
