import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'app/shared/auth/auth.guard';
import { UpdatePasswordComponent } from './update-password/update-password.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'app-update-password',
        canActivate: [AuthGuard],
        component: UpdatePasswordComponent,
        data: {
          title: 'Page',
          roles: ['Authenticated'],
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule { }
