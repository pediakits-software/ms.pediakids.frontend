import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { PedikidsService } from 'app/shared/api/pediakids.service';
import { InputType } from 'app/shared/components/generic-input/generic-input.component';
import { SweetAlertService } from 'app/shared/services/sweet-alert.service';
import { CrudOperation, Utils } from 'app/shared/utils/utils.resource';
import { Subscription } from 'rxjs';
import { Location } from "@angular/common"
import { UpdatePassword } from 'app/shared/api/pediakids.model';
import { LocalStorageService } from 'app/shared/localstorage/localstorage.service';
import { SecurityService } from 'app/shared/auth/security.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.scss']
})
export class UpdatePasswordComponent implements OnInit {
  _subscriptionData: Subscription;
  _accionCrudValue=CrudOperation.ADD_RECORD;
  inputType = InputType;
  updatePassword:UpdatePassword;
  initialNewPassword:string;
  updatePasswordForm:FormGroup;

  constructor(
    private _sweetAlertService:SweetAlertService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute, private location: Location,
    private _localStorageService:LocalStorageService,
    private _SecurityService:SecurityService,
  ) { }

  ngOnInit() {
    this.updatePassword=<UpdatePassword>{};
    this.updatePasswordForm = this.returnPatientForm();
    Utils.markFormGroupTouched(this.updatePasswordForm);
  }

  eventClickBack() {
    this.location.back();
  }
  
  returnPatientForm(): FormGroup {
    return this.formBuilder.group({
      IdUser: [this.updatePassword.IdUser?this.updatePassword.IdUser:0,[Validators.required]],
      CurrentPassword: [this.updatePassword.CurrentPassword,[Validators.required,Validators.minLength(5)]],
      InitialNewPassword: [this.initialNewPassword,[Validators.required,Validators.minLength(5)]],
      NewPassword: [this.updatePassword.NewPassword,[Validators.required,Validators.minLength(5)]],
    })
  }

  UpdatePassword(user:FormGroup){
    let localUser = this._localStorageService.getCurrentUserLocaStorage();
    let updateUser = user.getRawValue();
    if(updateUser.InitialNewPassword != updateUser.NewPassword){
      return this._sweetAlertService.showToastError("Las contraseñas nuevas no coinciden")
    }
    updateUser.IdUser = Utils.encrypt(localUser.Id);
    updateUser.CurrentPassword = Utils.encrypt(updateUser.CurrentPassword); 
    updateUser.NewPassword = Utils.encrypt(updateUser.NewPassword); 
    
    this._SecurityService.updatePassword(updateUser)
    .subscribe(
      (res: any) => {
        this._sweetAlertService.showToastSucces("Contraseña actualizada correctamente, la próxima vez que inicies sesión debes ingresar su nueva contraseña");
        this.eventClickBack();
      },
      (res: any) => {
        this.messageError(res.error.key??'');
      }
    );
  }

  messageError(code) {
    switch (code) {
      case 'USER_NOT_EXIST':
        this._sweetAlertService.showToastInfo(
          "El usuario no corresponde a una cuenta inscrita"
        );
      break;
      case 'PASSWORD_NOT_MATCH':
        this._sweetAlertService.showToastInfo(
          "La contraseña actual no es valida"
        );
      break;
      case 'OULD_NOT_REMOVE_PASSWORD':
        this._sweetAlertService.showToastInfo(
          "Lo sentimos, no se pudo remover la contraseña"
        );
      break;
      default:
        this._sweetAlertService.showToastInfo(
          "Ha ocurrido un error desconocido"
        );
        break;
    }
  }
}
