import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalConsultationByDateComponent } from './medical-consultation-by-date.component';

describe('MedicalConsultationByDateComponent', () => {
  let component: MedicalConsultationByDateComponent;
  let fixture: ComponentFixture<MedicalConsultationByDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MedicalConsultationByDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalConsultationByDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
