import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from "@angular/common";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DateRange } from 'app/shared/api/pediakids.model';
import { InputType } from 'app/shared/components/generic-input/generic-input.component';
import { SweetAlertService } from 'app/shared/services/sweet-alert.service';
import { Subject, Subscription } from 'rxjs';
import * as moment from 'moment';
import { takeUntil } from 'rxjs/operators';
import { PagedData } from 'app/shared/components/generic-datatable/model/paged-data';
import { MedicalConsultationService } from '../../MedicalConsultations/medical-consultation-list/medical-consultation.service';
import { BasePagination } from 'app/shared/odata/base-pagination';
import { PedikidsService } from 'app/shared/api/pediakids.service';

@Component({
  selector: 'app-medical-consultation-by-date',
  templateUrl: './medical-consultation-by-date.component.html',
  styleUrls: ['./medical-consultation-by-date.component.scss']
})
export class MedicalConsultationByDateComponent extends BasePagination implements OnInit, AfterViewInit, OnDestroy {
  datesForm: FormGroup;
  dateRange:DateRange;
  inputType = InputType;
  _subscriptionData: Subscription;
  medicalConsultationCount:number = -1;

  public _unsubscribeAll: Subject<any>;
  constructor(
    private route: ActivatedRoute, private location: Location,
    private _cd:ChangeDetectorRef,
    private sweetAlertService:SweetAlertService,
    private formBuilder: FormBuilder,
    private _medicalConsultationService: MedicalConsultationService,
    private _pediakidsService:PedikidsService,

  ) { 
    super()
    this._unsubscribeAll= new Subject()
  }
 
  ngOnInit(): void {
    this._subscriptionData = this.route.params.subscribe((parameters) => {
      this.dateRange=<DateRange>{};
      this.datesForm= this.returnDates();
      this.getCountMedicalConsultationById();
    });
  }

  ngAfterViewInit(): void {
    this.load();
  }

  ngOnDestroy(): void {
    if(this._unsubscribeAll){
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
      this._unsubscribeAll.unsubscribe();
    }
  }

  load(){
    this.getCountMedicalConsultationById();
  }

  getCountMedicalConsultationById(){
    this._medicalConsultationService.page.sortBy= `CreatedDate`
    this._medicalConsultationService.page.sortDir= `desc`
    this._medicalConsultationService.onChangeListMedicalConsultation
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((result: PagedData<any>) => {
      this._cd.detectChanges();
    });
    this._medicalConsultationService.getMedicalConsultation()
  }

  eventClickBack() {
    this.location.back();
  }

  returnDates(): FormGroup {
    return this.formBuilder.group({
      InitilDate: [this.dateRange.InitilDate, [Validators.required,Validators.nullValidator]],
      FinalDate: [this.dateRange.FinalDate, [Validators.required]],
    })
  }

  getMedicalConsultationByDate(dates: FormGroup){
   console.log(dates)
    if (isNaN(dates.value.InitilDate.getTime())) {
      return this.sweetAlertService.showToastError("La fecha inicial no es valida");
    }

    if (isNaN(dates.value.FinalDate.getTime())) {
      return this.sweetAlertService.showToastError("La fecha Final no es valida");
    }
    
    if(moment(new Date(dates.value.InitilDate)) > moment(new Date(dates.value.FinalDate)))
    {
      return this.sweetAlertService.showToastError("La fecha inicial no puede ser mayor a la final");
    }

    this._pediakidsService.getMedicalConsultationCustom(`CreatedDate gt ${dates.value.InitilDate.toISOString()} and CreatedDate lt ${dates.value.FinalDate.toISOString()}`, 10, 0, null, true, null, null, null)
      .then((result: any) => {
        this.medicalConsultationCount = result['@odata.count'];
      }, () => {
      });
  }

}