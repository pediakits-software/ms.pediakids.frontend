import { AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PagedData } from 'app/shared/components/generic-datatable/model/paged-data';
import { BasePagination } from 'app/shared/odata/base-pagination';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MedicalConsultationService } from '../../MedicalConsultations/medical-consultation-list/medical-consultation.service';
import { PatientListService } from '../../patient/patient-list/patient-list.service';

@Component({
  selector: 'app-report-view',
  templateUrl: './report-view.component.html',
  styleUrls: ['./report-view.component.scss']
})
export class ReportViewComponent extends BasePagination implements OnInit, AfterViewInit, OnDestroy {
  countPatients:number;
  countMedicalConsultations:number;

   //Suscribes
   public _unsubscribeAll: Subject<any>;
  constructor(
    private _router: Router,
    private _patientListService: PatientListService,
    private _medicalConsultationService: MedicalConsultationService,
    private _cd:ChangeDetectorRef,
  ) { 
      super()
      this._unsubscribeAll= new Subject()
  }

  ngOnInit(): void {
    this.getCountPatients();
    this.getCountMedicalConsultation();
  }

  ngOnDestroy(): void {
    if(this._unsubscribeAll){
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
      this._unsubscribeAll.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.getCountMedicalConsultation()
  }

  getCountPatients(){
    this._patientListService.page.sortBy= `Nombres`
    this._patientListService.page.sortDir= `asc`
    this._patientListService.page.expand= `Genero($select=Name)`
    this._patientListService.onChangeListPatients
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((result: PagedData<any>) => {
      this.countPatients = result.page.totalElements;
    });
    this._patientListService.getListPatients()
  }

  getCountMedicalConsultation(){
    this._medicalConsultationService.page.sortBy= `CreatedDate`
    this._medicalConsultationService.page.sortDir= `desc`
    this._medicalConsultationService.onChangeListMedicalConsultation
    .pipe(takeUntil(this._unsubscribeAll))
    .subscribe((result: PagedData<any>) => {
      this.countMedicalConsultations = result.page.totalElements;
      this._cd.detectChanges();
    });
    this._medicalConsultationService.getMedicalConsultation()
  }

  cardReport(){
    this._router.navigate(['paediakids/core/reports/medical-consultation-by-date']);
  }

}