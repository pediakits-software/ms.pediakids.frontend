import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/shared/auth/auth.guard';
import { MedicalConsultationByDateComponent } from './medical-consultation-by-date/medical-consultation-by-date.component';
import { ReportViewComponent } from './report-view/report-view.component';
const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        canActivate: [AuthGuard],
        component: ReportViewComponent,
        data: {
          title: 'Page',
          roles: ['Authenticated'],
        }
      },
      {
        path: 'medical-consultation-by-date',
        canActivate: [AuthGuard],
        component: MedicalConsultationByDateComponent,
        data: {
          title: 'Page',
          roles: ['Authenticated'],
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReportsRoutingModule { }

