import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportViewComponent } from './report-view/report-view.component';
import { ReportsRoutingModule } from './reports-routing.module';
import { MedicalConsultationByDateComponent } from './medical-consultation-by-date/medical-consultation-by-date.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenericInputDatepickerModule } from 'app/shared/components/generic-input-datepicker/generic-input-datepicker.module';



@NgModule({
  declarations: [
    ReportViewComponent,
    MedicalConsultationByDateComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    ReactiveFormsModule,
    GenericInputDatepickerModule,
    FormsModule,
  ]
})
export class ReportsModule { }
