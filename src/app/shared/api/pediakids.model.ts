
export interface Patient {
  Id?:number;
  Nombres?: string;
  Apellidos?: string;
  NumeroIdentificacion?: string;
  FechaNacimiento?: string,
  Direccion?: string;
  Telefono?: string;
  TipoIdentificacionId?:number;
  GeneroId?:number;
}

export interface UpdatePassword {
  IdUser?:string,
  CurrentPassword?:string,
  NewPassword:string
}

export interface TypeList{
  Id?: number;
  Name?: string;

}

export interface ItemList{
  Id?: number;
  Name?: string;
  Properties?: any,
  Code?: string,
  Active?:boolean,
  TypeListId?: number,
  Tipo_Parametro_Id?: number,
  Owner_Id?:number,
  TypeList:TypeList
}

export interface MedicalConsultation{
  Id?:number;
  Motivo?:String;
  EvolucionEnfermedad?:string,
  Antecedentes?:String;
  Peso?:string;
  Talla?:number;
  PerimetoCefalico?:number;
  FrecuenciaCardiaca?:number;
  FrecuenciaResiratorio?:number;
  Temperatura?:number;
  Saturacion?:number;
  TencionArterial?:string;
  Cabeza?:string;
  ORL?:string;
  Cuello?:string;
  Torax?:string;
  Abdomen?:string;
  Genitales?:string;
  Extremidades?:string;
  Piel?:string;
  Neurologico?:string;
  Diagnostico?:string;
  Tratamiento?:string,
  Paraclinicos?:string;
  ParaclinicosOpcional?:string;
  PatientId?:number;
  TipoIdentificacionId?:number;
  NumeroIdentificacion?:string;
}

export interface DateRange{
  InitilDate?: Date;
  FinalDate?: Date;
}


