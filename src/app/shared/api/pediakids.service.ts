import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';

import { ConfigService } from './config.service';
import { ODataClient } from '../odata/odata-client';
import * as models from './pediakids.model';
import { map } from 'rxjs/operators';
import { SecurityService } from '../auth/security.service';
import { LocalStorageService } from '../localstorage/localstorage.service';

@Injectable({
  providedIn: "root"
})
export class PedikidsService {
  odata: ODataClient;
  basePath: string;

  constructor(
    private _securityService:SecurityService,
    private _localStorageService:LocalStorageService,
    private http: HttpClient,
    private config: ConfigService) {
    this.basePath = config.get('pediakids');
    this.odata = new ODataClient(this.http, this.basePath, { legacy: false, withCredentials: true });
  }
  getPatients(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/Patients`, { filter, top, skip, orderby, count, expand, format, select });
  }

  createPatient(expand: string | null, patient: models.Patient | null) : Observable<any> {
    return this.odata.post(`/Patients`, patient, { expand }, []);
  }

  deletePatient(id: number | null) : Observable<any> {
    return this.odata.delete(`/Patients(${id})`, item => !(item.Id == id));
  }

  getPatiendById(expand: string | null, id: number | null) : Observable<any> {
    return this.odata.getById(`/Patients(${id})`, { expand });
  }

  updatePatient(expand: string | null, id: number | null, patient: models.Patient | null) : Observable<any> {
    return this.odata.patch(`/Patients(${id})`, patient, item => item.Id == id, { expand }, []);
  }

  getItemList(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/ItemLists`, { filter, top, skip, orderby, count, expand, format, select });
  }


  getMedicalConsultations(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null) : Observable<any> {
    return this.odata.get(`/MedicalConsultations`, { filter, top, skip, orderby, count, expand, format, select });
  }

  getMedicalConsultationCustom(filter: string | null, top: number | null, skip: number | null, orderby: string | null, count: boolean | null, expand: string | null, format: string | null, select: string | null):Promise<any> {
    return this.http.get(`${this.basePath}/MedicalConsultations?
    ${filter ? `$filter= ${filter}`:``}
    ${top ? `&$top=${top}`:``}
    ${skip ? `&$skip=${skip}`:``}
    ${orderby ? `&$orderby=${orderby}`:``}
    ${count ? `&$count=${count}`:``}
    ${expand ? `&$expand=${expand}`:``}
    ${select ? `&$select=${select}`:``}`)
    .pipe(map(res => {
      return res;
    })).toPromise();
  }
  
  getMedicalConsultationsById(id: number | null, expand: string ) : Observable<any> {
    var data = this.odata.getById(`/MedicalConsultations/GetMedicalConsultationByPatient(${id})`, { expand });
    return data;
  }

  createMedicalConsultation(expand: string | null, medicalConsultation: models.MedicalConsultation | null) : Observable<any> {
    return this.odata.post(`/MedicalConsultations`, medicalConsultation, { expand }, []);
  }



}
