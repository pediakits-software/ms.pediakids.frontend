
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Route, UrlSegment, UrlTree } from '@angular/router';
import { Injectable } from '@angular/core';
import { SecurityService } from './security.service';
@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private security: SecurityService) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
     const roles = route.data.roles as string[];
      return this.security.canActivate(roles);
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean | Promise<boolean | UrlTree> {
    const roles = route.data.roles as string[];
    return this.security.canActivate(roles);
  }
}
