import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Route, UrlTree, CanLoad, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { SecurityService } from './security.service';

@Injectable()

export class LoginGuard implements CanActivate, CanLoad {

  constructor( private security: SecurityService ) {}

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | boolean {
    return this.security.canActiveLogin();
  }

  canLoad(route: Route, segments: UrlSegment[]): boolean | Promise<boolean | UrlTree> {
    return this.security.canActiveLogin();
  }

}
