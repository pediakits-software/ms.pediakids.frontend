import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { SecurityService } from './security.service';

@Injectable()
export class SecurityInterceptor implements HttpInterceptor {
  constructor(private auth: SecurityService, private router: Router) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.indexOf(environment.pediakids) !== 0 && request.url.indexOf(environment.securityUrl) !== 0) {
      return next.handle(request);
    }

    if (this.auth.accessToken) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.auth.accessToken}`
        }
      });
    }

    return next.handle(request).pipe(catchError((response: any) => {
      /* if (response.status === 401) {
        const redirectUrl = this.router.url;
        this.router.navigate([{ outlets: { popup: null } }]).then(() =>
          this.router.navigate(['paediakids/auth/login'], { queryParams: { redirectUrl } })
        );
        response.error = {error: {message: 'Session expired.'}};
      } */
      return throwError(response);
    }));
  }
}



