import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, RouterStateSnapshot } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import { ODataClient } from '../odata/odata-client';
import { LocalStorageService } from '../localstorage/localstorage.service';

const TOKEN = 'id_token';
const NAME = 'unique_name';
const ROLE = 'role';

@Injectable({
  providedIn: "root"
})
export class UserService {
  name: string;
  roles: string[];
  profile: any;
  jwt = new JwtHelperService();

  constructor() {
    this.init();
  }

  isAuthenticated(): boolean {
    try {
      const token = localStorage.getItem(TOKEN);
      const expired = this.jwt.isTokenExpired( token );
      if (token && expired) {
        this.logout();
      }
      return !expired;
    } catch ( error ){
      this.logout();
      return false;
    }
  }

  isInRole(role: string | string[]): boolean {
    const roles = Array.isArray(role) ? role : [role];
    if (!this.isAuthenticated()) {
      return false;
    }

    if (roles.indexOf('Authenticated') > -1) {
      return true;
    }

    return roles.some((rol) => this.roles.indexOf(rol) > -1);
  }

  logout(): void {
    localStorage.removeItem(TOKEN);
    location.reload();
  }

  init(): void {
    let profile = { [NAME]: null, [ROLE]: null };

    if (this.isAuthenticated()) {
      profile = this.jwt.decodeToken(localStorage.getItem(TOKEN));
    }

    this.profile = profile;
    this.name = profile[ NAME ];
    this.roles = profile[ ROLE ];

    if (!this.roles) {
      this.roles = [];
    }

    this.roles = Array.isArray(this.roles) ? this.roles : [this.roles];
  }
}


@Injectable({
  providedIn: "root"
})
export class SecurityService {
  basePath = environment.securityUrl;
  odata: ODataClient;

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router,
    private http: HttpClient,
    public user: UserService
  ) {
    this.odata = new ODataClient(this.http, this.basePath, {
      legacy: false,
      withCredentials: true
    });
  }

  updatePassword(user:any):any{
    const headers: any =  { observe: 'response', headers: new HttpHeaders().set('Content-Type', 'application/json')};
    const body = JSON.stringify(user);
    return this.http.put(`${this.basePath}/UpdatePassword`, body, headers).pipe(map((result: any) => {
      if (result.status === 200) {
        return true;  
      }
    })).pipe( catchError((response) => {
        return throwError(response);
      })
    );
  };
  
  isAuthenticated(): boolean {
    return this.user.isAuthenticated();
  }

  isLoggedIn(): boolean {
    return this.isAuthenticated();
  }

  isInRole(role: string | string[]): boolean {
    return this.user.isInRole(role);
  }

  get profile(): any {
    return this.user.profile;
  }

  get role(): string {
    return this.user.roles[0];
  }

  get roles(): string[] {
    return this.user.roles;
  }

  get name(): string {
    return this.user.name;
  }

  get accessToken(): string {
    return localStorage.getItem(TOKEN);
  }

  get token(): string {
    return this.accessToken;
  }

  logout(): void {
    this.user.logout();
    this.localStorageService.logout();
  }

  canActivate(roles: string[]): boolean {
    if (this.isAuthenticated()) {
      if (this.isInRole(roles)) {
        return true;
      }
      this.router.navigate(['paediakids/auth/login']);
      return false;
    }

    this.router.navigate(['paediakids/auth/login']);
    return false;
  }


  canActiveLogin(): boolean {
    const autenticado = this.isAuthenticated();
    if (autenticado) {
      this.router.navigate(["/"]);
    }

    return !autenticado;
  }

  hasAnyAuthority(authorities: string[]): boolean {
    if (!this.isAuthenticated()) {
      return false;
    }
    return this.isInRole(authorities);
  }

  getUserCurrentLogued(): any{
    return this.localStorageService.getCurrentUserLocaStorage();
  }

  
  login(request: RequestLogin ): Observable<any> {
    const headers: any =  { observe: 'response', headers: new HttpHeaders().set('Content-Type', 'application/json')};
    const body = JSON.stringify(request);
    return this.http.post(`${this.basePath}/login`, body, headers).pipe(map((result: any) => {
      if (result.status === 200) {
        const accessToken  = result.headers.get('access_token');
        localStorage.setItem(TOKEN, accessToken);
        this.user.init();
        this.localStorageService.saveCurentUserLocalStorage(result.body);
        this.router.navigate(["/"]);
      }
    })).pipe( catchError((response) => {
        return throwError(response);
      })
    );
  }
}



export interface RequestLogin {
  username: string;
  password?: string;
  recordarme: boolean;
}
