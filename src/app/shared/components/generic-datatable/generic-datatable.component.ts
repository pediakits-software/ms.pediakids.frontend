import { Component, OnInit, ViewEncapsulation, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import {
  ColumnChangesService,
  ColumnMode,
  DatatableComponent,
  DimensionsHelper,
  ScrollbarHelper,
} from '@swimlane/ngx-datatable';
import { Page, SourceData } from './model/page';
import { Column } from './model/columns.model';
import { ActivatedRoute } from '@angular/router';
import { OdataFilter } from 'app/shared/odata/odata-filter-list';
import { sortBy } from 'lodash';

@Component({
  selector: 'app-generic-datatable',
  templateUrl: './generic-datatable.component.html',
  styleUrls: ['./generic-datatable.component.scss', '../../../../assets/sass/libs/datatables.scss'],
  encapsulation: ViewEncapsulation.None,
  providers:[ScrollbarHelper,DimensionsHelper, ColumnChangesService]
})
export class GenericDatatableComponent implements OnInit {


  sourceData= SourceData

  idSearch= Math.random();

  // row data
  @Input() public rows = [];
  // column header
  @Input() public columns:Array<Column> = [];

  // column header
  @Input() public filterOdataProperties = [];
  @Input() public filterCombineTwoProperties = [];


  @Output() nextPage = new EventEmitter<any>();
  @Output() filterEvent = new EventEmitter<any>();
  @Output() filterOdataEvent = new EventEmitter<any>();
  @Output() sortEvent = new EventEmitter<any>();
  @Output() limitPageEvent = new EventEmitter<any>();
  @Output() eventClickButton = new EventEmitter<any>();


  @Input() page = new Page();
  currentPageLimit=this.page.itemsPerPage;
  @Input() enabledFilter = true;
  @Input() enabledButton = false;
  @Input() titleButton: String = "Agregar";


  public ColumnMode = ColumnMode;

  public pageLimitOptions = [
      {value: 10},
      {value: 20},
      {value: 50},
      {value: 100},
  ];

  @ViewChild(DatatableComponent) table: DatatableComponent;

  /**
   * filterUpdate
   *
   * @param code
   */

  filter(event) {
    let query= null;
    let queryCombine= null;
    if(this.filterOdataProperties && this.filterOdataProperties.length>0 && event.target.value){
        this.filterOdataProperties.forEach((item, index)=>{
          let propertyReplace = OdataFilter.replaceAll(item);
          if(index==0 ){
            query= ` contains( tolower(${propertyReplace}), tolower('${event.target.value}'))`
          } else{
            query+= ` or contains( tolower(${propertyReplace}), tolower('${event.target.value}')) `
          }
        })
    }

    if(this.filterCombineTwoProperties && this.filterCombineTwoProperties.length==2 && event.target.value ){
      const valueSearch = event.target.value.replace( /\s/g, '')
      query=query? `( ${query} or contains(concat(concat(tolower(${this.filterCombineTwoProperties[0]}),''), tolower(${this.filterCombineTwoProperties[1]})), tolower('${valueSearch}')))`:
                  ` ( contains(concat(concat(tolower(${this.filterCombineTwoProperties[0]}),''), tolower(${this.filterCombineTwoProperties[1]})), tolower('${valueSearch}')))`
    }else{
      query= query ? `( ${query} )`:query
    }

    this.filterOdataEvent.next(query);
    this.filterEvent.next(event.target.value);
    this.table.offset = 0;
  }


  /**
   * Constructor
   *
   * @param {ActivatedRoute} _route
   */
  constructor(    private _route: ActivatedRoute,
    ) {
  }

   /**
   * Populate the table with new data based on the page number
   * @param page The page to select
   */
  setPage(pageInfo) {
    this.page.currentPage = pageInfo.offset;
    this.nextPage.next(this.page);
  }
  public noopComparator(): number {
    return 0;
 }

 dynamicSort(property, order) {
  var sortOrder = order == "desc" ? -1:1;
  return function (a,b) {
      /* next line works with strings and numbers,
       * and you may want to customize it to your needs
       */
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
  }
}

  onSort(event){
    if(this.page.sourceData===this.sourceData.LOCAL){
      if(event.column.propSortable){
        this.page.sortBy=event.column.propSortable
      }else{
        this.page.sortBy=event.sorts[0].prop
      }
      this.rows.sort(this.dynamicSort(this.page.sortBy, event.newValue));
    }else{
      if(event.column.propSortable){
        let propertyReplace = event.column.propSortable.replace(".", "/");
        this.page.sortBy=propertyReplace
      }else{
        let propertyReplace = event.sorts[0].prop.replace(".", "/");
        this.page.sortBy=propertyReplace
      }
      this.page.sortDir=event.sorts[0].dir;
      this.sortEvent.next(this.page);
    }

  }

  onLimitChange(limit){
    this.page.itemsPerPage = parseInt(limit);
    this.page.currentPage= 0
    this.limitPageEvent.next(this.page)
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit() {
  }

  onEventClick(){
    this.eventClickButton.next()
  }

}
