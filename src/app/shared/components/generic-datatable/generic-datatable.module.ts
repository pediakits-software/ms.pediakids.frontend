import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericDatatableComponent } from './generic-datatable.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PipeModule } from 'app/shared/pipes/pipe.module';



@NgModule({
  declarations: [
    GenericDatatableComponent
  ],
  imports: [
    CommonModule,
    NgxDatatableModule,
    PipeModule,
  ],
  exports:[GenericDatatableComponent]

})
export class GenericDatatableModule { }
