export interface Column{
  name:string;
  prop?:string;
  propSortable?:string;
  cellTemplate?:any;
  sortable:boolean;
  width?:number;
  maxWidth?:number;
  splitProps?:Array<any>; // example: ['city', 'state']
  flexGrow?:number;  // exmaple : 1 or 0.5
  resizeable?:boolean; //false or true
  headerClass?:string;   //style the column headers
  cellClass?:string;
  pipe?:any;
}

