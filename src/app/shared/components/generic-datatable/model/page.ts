/**
 * An object used to get page information from the server
 */
export class Page {
  // The number of elements in the page
  itemsPerPage: number = 10;
  // The total number of elements
  totalElements: number = 0;
  // The total number of pages
  totalPages: number = 0;
  // The current page number
  currentPage: number = 0;
  sortBy: string="Id";
  sortDir: string="desc";
  filterQuery: string =null;
  filterSearch: string =null;
  expand: string =null;
  select: string =null;
  sourceData: SourceData =SourceData.SERVER;


  get getFilterQueryComplete() {
    let query=null;
    query = this.filterQuery ? this.filterQuery:null
    query = query ? (this.filterSearch ? `${query} and ${this.filterSearch}`
                                      :query)
                  :this.filterSearch ? this.filterSearch : null
    return query
  }
}



export enum SourceData{
  SERVER="SERVER",
  LOCAL="LOCAL"
}
