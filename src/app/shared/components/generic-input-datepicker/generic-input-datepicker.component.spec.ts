import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericInputDatepickerComponent } from './generic-input-datepicker.component';

describe('GenericInputDatepickerComponent', () => {
  let component: GenericInputDatepickerComponent;
  let fixture: ComponentFixture<GenericInputDatepickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenericInputDatepickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericInputDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
