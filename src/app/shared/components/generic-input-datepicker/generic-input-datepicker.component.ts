import { ChangeDetectorRef, Component, EventEmitter, forwardRef, Input, OnInit, Output } from '@angular/core';
import { ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ShowErrorsService } from 'app/shared/validators/show-errors.service';
import * as moment from 'moment';

@Component({
  selector: 'app-generic-input-datepicker',
  templateUrl: './generic-input-datepicker.component.html',
  styleUrls: ['./generic-input-datepicker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => GenericInputDatepickerComponent),
      multi: true
    }
  ],
})
export class GenericInputDatepickerComponent implements   OnInit,ControlValueAccessor{

  model: any;
  @Input() required= false;
  @Input() title=""
  @Input() showIcon= false;
  @Input() icon:any= "ft-user";
  @Input() mask="";

  @Input() formControlName:any = null;
  @Input() formGroupNameError=null;
  @Input() formGroup: FormGroup;
  @Input() formGroupValidator: FormGroup;
  @Input() showTextError= true;
  @Input() inputSizeClass='form-control-sm'; // form-control-lg  (or) form-control-sm

  @Output() eventChange = new EventEmitter<Date>();

  constructor(
    private _cd:ChangeDetectorRef,
    public showErrorsService:ShowErrorsService,) {
  }

  ngOnInit(): void {
    if(this.formGroup){
      let value= this.formGroup.get(this.formControlName).value
      if(value){
        let date= moment(value).format("YYYY-MM-DD")
        this.model= this.formatDate(date)
        this._cd.detectChanges()
        return;
      }
    }
    this.model= this.formatDate(new Date())
    this.onChange(this.model);
  }

  private formatDate(date) {
    const d = new Date(date);
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();
    const year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  onChange(date){
    if(this.formGroup && this.formControlName){
      this.formGroup.get(this.formControlName).markAsDirty()
    }

    //Se marca como markAsDirty una vez se selecciona una fecha
    if(this.formGroupValidator && this.formGroupNameError){
      this.formGroupValidator.get(this.formGroupNameError).markAsDirty()
    }

    if(this.formGroup){
      var dateMOment = moment(`${date}T00:00:00Z`,'YYYY-MM-DD').toDate();
      this.formGroup.get(this.formControlName).patchValue(dateMOment)
      this.eventChange.next(dateMOment);
    }
  }


  writeValue(obj: any): void {
    //throw new Error("Method not implemented.");
  }
  registerOnChange(fn: any): void {
    //throw new Error("Method not implemented.");
  }
  registerOnTouched(fn: any): void {
    //throw new Error("Method not implemented.");
  }
  setDisabledState?(isDisabled: boolean): void {
    //throw new Error("Method not implemented.");
  }

  errorEvent(event){
  }
}
