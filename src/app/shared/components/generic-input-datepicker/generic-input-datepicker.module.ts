import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericInputDatepickerComponent } from './generic-input-datepicker.component';
import { NgbDatepickerModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'




@NgModule({
  declarations: [
    GenericInputDatepickerComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NgxMaskModule.forChild()

  ],
  exports:[GenericInputDatepickerComponent]
})
export class GenericInputDatepickerModule { }
