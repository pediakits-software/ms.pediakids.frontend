import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericLoadingComponent } from './generic-loading.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { GenericLoadingService } from './generic-loading.service';



@NgModule({
  declarations: [
    GenericLoadingComponent
  ],
  imports: [
    CommonModule,
    NgxSpinnerModule
  ],
  providers:[GenericLoadingService],
  exports:[GenericLoadingComponent]


})
export class GenericLoadingModule { }
