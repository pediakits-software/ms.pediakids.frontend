import { TestBed } from '@angular/core/testing';

import { GenericLoadingService } from './generic-loading.service';

describe('GenericLoadingService', () => {
  let service: GenericLoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GenericLoadingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
