import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})
export class GenericLoadingService {
  isLoading:boolean= false;
  constructor(private spinner: NgxSpinnerService,) { }
  show(){
    this.isLoading= true;
    this.spinner.show()

  }
  hide(){
    this.isLoading= false;
    this.spinner.hide();

  }
}
