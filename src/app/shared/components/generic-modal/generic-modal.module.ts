import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericModalComponent } from './generic-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [GenericModalComponent],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports:[GenericModalComponent]
})
export class GenericModalModule { }
