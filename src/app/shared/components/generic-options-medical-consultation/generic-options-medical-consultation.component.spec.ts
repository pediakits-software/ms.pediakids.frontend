import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericOptionsMedicalConsultationComponent } from './generic-options-medical-consultation.component';

describe('GenericOptionsMedicalConsultationComponent', () => {
  let component: GenericOptionsMedicalConsultationComponent;
  let fixture: ComponentFixture<GenericOptionsMedicalConsultationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenericOptionsMedicalConsultationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericOptionsMedicalConsultationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
