import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  Injector} from "@angular/core";

import { Subscription } from "rxjs";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-generic-options-medical-consultation',
  templateUrl: './generic-options-medical-consultation.component.html',
  styleUrls: ['./generic-options-medical-consultation.component.scss']
})
export class GenericOptionsMedicalConsultationComponent implements OnInit {
  _parameters: any;
  _subscription: Subscription;

  //Event buttons
  @Output() printerEventClick = new EventEmitter<any>();
  @Output() viewHistoryEventClick = new EventEmitter<any>();

 /*  @ViewChild('buttonOpciones', {static: false})
  public buttonOpciones:ElementRef; */
  @Input() data: any = null;
  selectedButton: string;

  constructor(
    private _cd: ChangeDetectorRef,
    private injector: Injector,
    private _route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this._subscription = this._route.params.subscribe(parameters => {
      this._parameters = parameters;
      this._cd.detectChanges();
    });
  }


  onClickItem(event:string){
    if (event) {
      switch (event) {
        case 'PRINTER': {
          this.printerEventClick.emit(this.data);
          break;
        }
        case  'VIEW_HISTORY': {
          this.viewHistoryEventClick.emit(this.data);
          break;
        }
        default: {
          //statements;
          break;
        }
      }
    }
  }

}
