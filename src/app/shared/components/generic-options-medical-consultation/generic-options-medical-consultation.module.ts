import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericOptionsMedicalConsultationComponent } from './generic-options-medical-consultation.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [
    GenericOptionsMedicalConsultationComponent
  ],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports:[GenericOptionsMedicalConsultationComponent]

})
export class GenericOptionsMedicalConsultationModule { }
