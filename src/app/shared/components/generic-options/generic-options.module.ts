import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericOptionsComponent } from './generic-options.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';



@NgModule({
  declarations: [ 
    GenericOptionsComponent
  ],
  imports: [
    CommonModule,
    NgbModule
  ],
  exports:[GenericOptionsComponent]

})
export class GenericOptionsModule { }
