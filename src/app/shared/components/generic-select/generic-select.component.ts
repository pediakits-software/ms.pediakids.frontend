import { ChangeDetectorRef, EventEmitter, forwardRef, Output, SimpleChanges } from '@angular/core';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ShowErrorsService } from 'app/shared/validators/show-errors.service';

@Component({

  selector: 'app-generic-select',
  templateUrl: './generic-select.component.html',
  styleUrls: ['./generic-select.component.scss', '../../../../assets/sass/libs/select.scss'],
 // styleUrls: ['./generic-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => GenericSelectComponent),
      multi: true,
    },
  ],
  encapsulation: ViewEncapsulation.None
})
export class GenericSelectComponent implements OnInit, ControlValueAccessor {

  selectTypeResource = SelectType;
  @Input() selectType = SelectType.SELECT;
  @Input() title: String = "";
  @Input() placeholder: String = "Seleccione una opcion";

  @Input() formControlName: any = null;
  @Input() formGroupNameError = null;
  @Input() formGroup: FormGroup;
  @Input() formGroupValidator: FormGroup;
  @Input() items: ItemSelect[] = [];
  @Input() searchable: boolean = true;



  @Input() icon:any= "ft-chevrons-down";

  @Input() appendTo= ''; //body

  @Input() idSelect:any= this.formControlName;
  @Input() showIcon= false;
  @Input() showTextError= true;
  @Input() captureValue= true;

  @Input() required= false;


  @Input() defaultValue:number;



  @Output() onChangeSelect = new EventEmitter<any>();

  constructor(
    private _cd:ChangeDetectorRef,
    public showErrorsService: ShowErrorsService,) {
  }
  writeValue(obj: any): void {
    //throw new Error("Method not implemented.");
  }
  registerOnChange(fn: any): void {
    //throw new Error("Method not implemented.");
  }
  registerOnTouched(fn: any): void {
    //throw new Error("Method not implemented.");
  }
  setDisabledState?(isDisabled: boolean): void {
    //throw new Error("Method not implemented.");
  }
  ngOnInit() {
    if(this.formGroup && this.formControlName){
      this.defaultValue=this.formGroup.get(this.formControlName).value
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
   /*  if(changes['items']){
      this._cd.detectChanges()
    } */
  }

  onChange($event){
    if(Array.isArray($event)){
      //let itemsSelected=  this.items.filter(a => $event.some(b=>b.value===a.value));
      let itemsSelected=  this.items.filter(a => $event.includes(a.value));
      this.onChangeSelect.next(itemsSelected)
    }else{
      let itemSelected=  this.items.find(a =>a.value== $event);
      this.onChangeSelect.next(itemSelected)
    }
  }
}


export enum SelectType {
  SELECT = "select",
  MULTISELECT = "multiselect"
}

export interface ItemSelect {
  data?: any;
  name: string;
  value: any;
  selected?: boolean;
  disabled?:boolean;
}

