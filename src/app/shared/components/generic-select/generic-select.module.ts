import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericSelectComponent } from './generic-select.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';



@NgModule({
  declarations: [
    GenericSelectComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule
  ],
  exports:[GenericSelectComponent]
})
export class GenericSelectModule { }
