import { Page, SourceData } from "../components/generic-datatable/model/page";

export abstract class BasePagination implements IPagination {

  page =new Page()
  constructor( ){
  }

  filterOdataEvent(query: string): void {
    this.page.currentPage= 0;
    this.page.filterSearch= query;
    (<any>this).onFilterOdataEvent()
  }


  nexPageEvent(event:Page): void {
    if( this.page.totalElements>this.page.itemsPerPage){
      this.page.currentPage = event.currentPage;
      (<any>this).onNexPageEvent(true)
      return;
    }
    (<any>this).onNexPageEvent(false)
  }
  sortEvent(event:Page): void {
    this.page.sortBy=event.sortBy;
    this.page.sortDir= event.sortDir;
    (<any>this).onSortEvent()
  }
  limitPageEvent(event:Page): void {
    this.page.itemsPerPage=event.itemsPerPage;
    this.page.currentPage= event.currentPage;
    (<any>this).onLimitPageEvent()
  }
}

export abstract class  IListenPagination {
  abstract onNexPageEvent(nextPage:boolean|null): void;
  abstract onSortEvent(): void;
  abstract onLimitPageEvent(): void;
  abstract onFilterOdataEvent(): void;
}


export abstract class  IPagination {
  abstract nexPageEvent(event:Page): void;
  abstract sortEvent(event:Page): void;
  abstract limitPageEvent(event:Page): void;
  abstract filterOdataEvent(query:string|null): void;
}
