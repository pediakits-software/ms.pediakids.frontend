import { Page } from "../components/generic-datatable/model/page";

export abstract class OdataFilter {
  static getCustomFilter(list: any): string {
    let filter = "";
    for (let item of list) {
      let propertyReplace = this.replaceAll(item.property);
      //Validacion de contains para Strings
      if (item.operator == "contains") {
        if (filter == "") {
          filter += `contains( tolower(${propertyReplace} ), tolower('${item.value}'))`
        } else {
          filter += ` and contains( tolower(${propertyReplace} ), tolower('${item.value}'))`
        }
      }
      if (item.operator == "equal") {
        if (filter == "") {
          filter += `${propertyReplace} eq ${item.value}`
        } else {
          filter += ` and ${propertyReplace} eq ${item.value}`
        }
      }
    }
    return filter;
  }

  static replaceAll(string: String): String {
    return string.split('.').join('/');
  }



  static getCustomFilterUsuario(list: any,filterNombre:string, filterApellido:string): string {

    let filter = "";
    console.log(list)
    for (let item of list) {
      //Validacion de contains para Strings
      if (item.operator == "contains") {
        //console.log("FILTER: ",item.property);
        if(item.property=="Usuario"){
          if (filter == "") {
            let propertyReplace = this.replaceAll(item.property);
            filter += `contains(concat(concat(tolower(${filterNombre}),' '), tolower(${filterApellido})), tolower('${item.value}'))`;
          } else {
            let propertyReplace = this.replaceAll(item.property);
            filter += ` and contains(concat(concat(tolower(${filterNombre}),' '), tolower(${filterApellido})), tolower('${item.value}'))`;
          }
        }else{
          if (filter == "") {




            let propertyReplace = this.replaceAll(item.property);
            filter += `contains( tolower(${propertyReplace} ), tolower('${item.value}'))`
          } else {
            let propertyReplace = this.replaceAll(item.property);
            filter += ` and contains( tolower(${propertyReplace} ), tolower('${item.value}'))`
          }
        }


      }
      if (item.operator == "equals") {
        if (filter == "") {
          let propertyReplace = this.replaceAll(item.property);
          filter += `${propertyReplace} eq ${item.value}`
        } else {
          let propertyReplace = this.replaceAll(item.property);
          filter += ` and ${propertyReplace} eq ${item.value}`
        }
      }
    }
    return filter;
  }
}
