import { TestBed } from '@angular/core/testing';

import { OdataPaginationService } from './odata-pagination.service';

describe('OdataPaginationService', () => {
  let service: OdataPaginationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OdataPaginationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
