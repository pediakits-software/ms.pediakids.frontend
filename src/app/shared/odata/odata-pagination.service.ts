import { Injectable } from '@angular/core';
import { Page } from '../components/generic-datatable/model/page';
import { PagedData } from '../components/generic-datatable/model/paged-data';

@Injectable({
  providedIn: 'root'
})
export class OdataPaginationService {

  constructor() { }

   /**
   * Package companyData into a PagedData object based on the selected Page
   * @param page The page data used to get the selected data from companyData
   * @returns {PagedData<any>} An array of the selected data and page
   */
  getPagedData(item:any, event: Page, currentPage:Page|null, countProperty:string='@odata.count', ): PagedData<any> {
    const pagedData = new PagedData<any>();
    let pages: number = event.totalElements != null && event.itemsPerPage != null ? Math.round(event.totalElements / event.itemsPerPage) : event.totalElements;
    event.totalElements= event.itemsPerPage != null && event.currentPage != null ? item[countProperty] : item.value.length;
    event.totalPages = pages;
    pagedData.data= item.value;
    pagedData.page = event;
    this.updatePage(event,currentPage) //Update page from service remote
    return pagedData;
  }

  updatePage(newPage:Page, currentPage:Page){
    currentPage.currentPage=newPage.currentPage
    currentPage.filterQuery=newPage.filterQuery
    currentPage.itemsPerPage=newPage.itemsPerPage
    currentPage.sortBy=newPage.sortBy
    currentPage.sortDir=newPage.sortDir
    currentPage.totalElements=newPage.totalElements
    currentPage.totalPages=newPage.totalPages
  }
}
