import { TestBed } from '@angular/core/testing';

import { ParaclinicalReportService } from './paraclinical-report.service';

describe('ParaclinicalReportService', () => {
  let service: ParaclinicalReportService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParaclinicalReportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
