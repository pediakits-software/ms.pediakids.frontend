import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class SweetAlertService {

  constructor() { }


  showAlertConfirmation(title: string, text: string, type: any, confirmButtonText: string, showCancelButton: boolean, cancelButtonText: string) {
    return Swal.fire({
      title: title,
      html:text,
      type:type,
      showCancelButton:showCancelButton,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText:confirmButtonText,
      cancelButtonText:cancelButtonText
    }).then((result) => {
      return result.value
    })
  }

  showAlert(title: string, text: string, type: any, width:any | null=null) {
    return Swal.fire({
      type: type,
      title: title,
      html:text,
      width: width?width:null,
      /* footer: '<a href>Why do I have this issue?</a>' */
    })
  }

  handledDeleteAlert(Update?:Boolean){
    if(Update == true){
      Swal.fire(
        'Editado!',
        'El registro fue editado.',
        'success'
      )
    }else{
      Swal.fire(
        'Eliminado!',
        'El registro fue eliminado.',
        'success'
      )
    }
  }

  showToastSucces(message: string | null = null){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    Toast.fire({
      type: 'success',
      title: message?`${message}`:`La solicitud se realizó correctamente!`
    })
  }

  showToastError(message: string | null){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 4000
    });

    Toast.fire({
      type: 'error',
      title: message?`${message}`:`Ha ocurrido un error!`
    })
  }


  showToastInfo(message: string | null){
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    Toast.fire({
      type: 'info',
      title: message?`${message}`:``
    })
  }

}
